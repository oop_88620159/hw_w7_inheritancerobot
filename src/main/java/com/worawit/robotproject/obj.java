/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author acer
 */
public class obj {
    protected int x;
    protected  int y;
    protected  char symbol;

    public obj(char symbol,int x, int y) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }
    
    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
    public String toString(){
        return "Robot>> "+"x="+ x +","+ " y="+y+"," ;
    }
}
