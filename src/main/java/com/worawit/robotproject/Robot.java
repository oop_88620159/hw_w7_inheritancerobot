/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author werapan
 */
public class Robot extends obj{
    private TableMap map;
    int fuel ;
    public Robot(int x, int y, char symbol, TableMap map, int fuel) {
        super(symbol,x,y);
        this.map = map;
        this.fuel = fuel;
    }
    public boolean walk(char direction) {
        switch (direction) {
            case 'N': 
            case 'w':
                if (walkN()) return false;
                break;
            case 'S':
            case 's':
                if (walkS()) return false;
                break;
            case 'E':
            case 'd':
                if (walkE()) return false;

                break;
            case 'W':
            case 'a':
                if (walkW()) return false;
                break;
            default:
                return false;
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if(map.isBomb(x, y)) {
            System.out.println("Founded Bomb!!!! (" + x + ", " + y + ")");
        }
    }
    private  boolean canwalk(int x, int y){
        return fuel>0 && map.inMap(x , y) && !(map.isTree(x, y));
    }
    private void reducefuel(){
        fuel--;
    }
    private boolean walkW() {
        checkFuel();
        if (canwalk(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        reducefuel();
        return false;
    }

    public void checkFuel() {
        int fuel = map.fillFuel(x, y);
        if(fuel>=0){
            this.fuel +=fuel;
        }
    }

    private boolean walkE() {
        checkFuel();
        if (canwalk(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
        reducefuel();
        return false;
    }

    private boolean walkS() {
        checkFuel();
        if (canwalk(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        reducefuel();
        return false;
    }

    private boolean walkN() {
        checkFuel();
        if (canwalk(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        reducefuel();
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
    public String toString(){
        return super.toString()+"Fuel="+ fuel +")";
    }
}
